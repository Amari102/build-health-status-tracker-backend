let
nixpkgs = import (builtins.fetchTarball https://github.com/NixOS/nixpkgs/archive/nixos-23.05.tar.gz) {
  overlays = [];
  config = {};
};

in
with nixpkgs;

stdenv.mkDerivation {
  name = "build-health-status-tracker-backend-env";
  buildInputs = [];

  nativeBuildInputs = [
    # Utilities
    fish
    fzf
    starship
    emacs-nox
    nixpkgs-fmt

    # Fixes
    glibcLocales  # Fix for initdb: error: invalid locale name
    
    # PostgreSQL
    postgresql_12
    pgadmin4
    
    # Python
    poetry
    python311
  ];
}
