"""Tests that can interact with API database.

In the current setup, database is mocked with SQLite. The database
is resetted between every test.
"""

from typing import Generator

import pytest
from flask import Flask
from werkzeug.test import Client

from src.app import AppWithRoutes, Config, create_flask_app, db
from src.extensions import db
from src.models.device_model import DeviceModel
from src.resources.device_model import DeviceModelResource, DeviceModelsListResource


@pytest.fixture(scope="function", name="app")
def fixture_app() -> Generator[AppWithRoutes, None, None]:
    app: AppWithRoutes = create_flask_app(config=TestAppConfig)
    app.testing = True

    with app.test_client() as _client:
        # with app.app_context():
        #     pass
        # db.create_all()
        yield Client(app)


class TestAppConfig(Config):
    TESTING = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "sqlite://"
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestEnv:
    class Endpoints:
        device_models = "/device_models"
        device_model = "/device_model"

    class Databases:
        test_sqlite_db = db

    endpoints = Endpoints()
    databases = Databases()


@pytest.fixture(scope="session", name="test_env")
def fixture_test_env():
    return TestEnv()


# 🗒️ 9b6783 TODO: add tests for autoincrement id


@pytest.mark.block_network
class TestDeviceModels:
    def test_retrieving_device_models(self, app, test_env):
        response = app.get(test_env.endpoints.device_models)
        assert response.status_code == 200

    @pytest.mark.modifies_database
    def test_create_device_model(self, app, test_env):
        example_device = {"name": "Aérospatiale/BAC Concorde"}
        response = app.post(test_env.endpoints.device_models, json=example_device)
        assert response.status_code == 201
        expected_response = {
            "id": 1,
            "name": "Aérospatiale/BAC Concorde",
        }
        assert response.json == expected_response

    @pytest.mark.modifies_database
    def test_delete_device_model(self, app, test_env):
        device_name = "Aérospatiale/BAC Concorde"
        device_model = DeviceModel(name=device_name)
        database = test_env.databases.test_sqlite_db

        with app.application.app_context():
            database.session.add(device_model)
            database.session.commit()

        response = app.delete(test_env.endpoints.device_model + "/1")
        assert response.status_code == 204
        response = app.delete(test_env.endpoints.device_model + "/1")
        assert response.status_code == 404

    def test_delete_nonexistent_device_model(self, app, test_env):
        response = app.delete(test_env.endpoints.device_model + "/9999999")
        assert response.status_code == 404


@pytest.mark.block_network
class TestApi:
    def test_retrieving_nonexistent_endpoint(self, app):
        response = app.get("/nonexistent_endpoint")
        assert response.status_code == 404
        assert response.status_code == 404
