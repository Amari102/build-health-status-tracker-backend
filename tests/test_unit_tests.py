import pytest
from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

from src.app import (
    apply_app_configuration,
    create_new_app,
    get_routes_list,
    register_app_in_db,
    register_routes,
)
from src.config import Config
from src.resources.device_model import DeviceModelResource, DeviceModelsListResource


@pytest.mark.block_network
class TestUnitTests:
    def test_applying_valid_app_configuration(self):
        class CustomConfig(Config):
            TESTING = True
            SQLALCHEMY_DATABASE_URI = "Banana"

        flask_app: Flask = create_new_app(__name__)
        apply_app_configuration(flask_app, config=CustomConfig)

        config = flask_app.config
        assert config["TESTING"] is True
        assert config["SQLALCHEMY_DATABASE_URI"] == "Banana"

    def test_registering_routes(self):
        flask_app: Flask = create_new_app(__name__)
        api = Api(flask_app)
        routes = [
            (DeviceModelResource, "/device_model/<string:device_model_id>"),
            (DeviceModelsListResource, "/device_models"),
        ]
        register_routes(api, routes)

        assert "/device_model/<device_model_id>" in str(vars(flask_app))
        assert "/device_models" in str(vars(flask_app))

        assert "devicemodelresource" in api.endpoints
        assert "devicemodelslistresource" in api.endpoints
