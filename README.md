### Fix Pythonpath
```sh
export PYTHONPATH="$PYTHONPATH:$PWD"
```

### Run Flask server
```sh
python3 src/app.py
```

### Initialize database
```sh
cd src/
python3 -m flask db init
```

### Initialize database server
```sh
cd database_setup_scripts/
./init_database.sh
./create_db.sh
```

### Start database server
```sh
cd database_setup_scripts/
./start_db_server.sh
```

### Create database and tables
Database server must be running.
```sh
cd src/
python3 -m flask db migrate
```

### Upgrade database
This will upgrade database to conform with models specification.
```sh
cd src/
# python3 -m flask db stamp head
python3 -m flask db migrate
# python3 -m flask db upgrade --sql  # For extra information
python3 -m flask db upgrade
```

### Get current revision
```sh
cd src/
python3 -m flask db current
```

### Create user
```sh
psql -p 5556 -h localhost build_status_db
```
```
build_status_db=# create user your_name with superuser password 'your_name';
```
