from dataclasses import dataclass
from typing import List, NewType, Type, TypeAlias, cast

import flask_restful
from flask import Flask
from flask_cors import CORS
from flask_migrate import Migrate
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

from src.config import Config
from src.extensions import db

# from src.models.device_model import DeviceModel
from src.resources.device_model import DeviceModelResource, DeviceModelsListResource

Route: TypeAlias = tuple[Type[flask_restful.Resource], str]

AppWithConfiguration = NewType("AppWithConfiguration", Flask)
AppRegisteredWithDB = NewType("AppRegisteredWithDB", Flask)
AppWithRoutes = NewType("AppWithRoutes", Flask)


def create_new_app(name: str) -> Flask:
    return Flask(name)


def apply_app_configuration(app: Flask, config: Type[Config]) -> AppWithConfiguration:
    """Create Flask app with applied configuration.

    Mutates app passed in parameter instead of returning modified app.
    """
    app.config.from_object(config)
    return cast(AppWithConfiguration, app)


def register_app_in_db(
    app: AppWithConfiguration, database: SQLAlchemy
) -> AppRegisteredWithDB:
    database.init_app(app)
    migrate = Migrate(app, database, compare_type=True)
    with app.app_context():
        database.create_all()
    return cast(AppRegisteredWithDB, app)


def add_routes_to_app(app: AppRegisteredWithDB, routes: List[Route]):
    api = Api(app)
    register_routes(api, routes=routes)
    return cast(AppWithRoutes, app)


def register_routes(api: flask_restful.Api, routes: List[Route]):
    for route_resource, route_path in routes:
        api.add_resource(route_resource, route_path)


def create_flask_app(config: Type[Config]) -> AppWithRoutes:
    app: Flask = create_new_app(name=__name__)
    app_with_config: AppWithConfiguration = apply_app_configuration(app, config=config)
    app_with_db: AppRegisteredWithDB = register_app_in_db(app_with_config, database=db)
    app_with_routes: AppWithRoutes = add_routes_to_app(app_with_db, get_routes_list())

    return app_with_routes


def get_routes_list() -> List[Route]:
    return [
        (DeviceModelResource, "/device_model/<int:device_model_id>"),
        (DeviceModelsListResource, "/device_models"),
    ]


# This definition needs to be global to enable "flask db migrate" to run properly.
app = create_flask_app(config=Config)

if __name__ == "__main__":
    cors = CORS(
        app,
        resources={r"/*": {"origins": "*"}},
    )
    app.run()
