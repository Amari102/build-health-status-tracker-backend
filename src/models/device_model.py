from typing import Any, Optional

from sqlalchemy.ext.declarative import DeclarativeMeta

# from sqlalchemy.orm.query import Query
from src.extensions import db


class DeviceModel(db.Model):
    __tablename__ = "device_model"

    device_model_id = db.Column(
        # "id", db.Integer, db.Identity(start=0, cycle=True), primary_key=True
        "id",
        db.Integer,
        primary_key=True,
        autoincrement=True,
        nullable=False,
    )
    name = db.Column("name", db.String(60), nullable=False, unique=True)
    is_deprecated = db.Column(db.Boolean(), nullable=False, default=False)

    def save(self) -> None:
        db.session.add(self)
        db.session.commit()

    def data(self) -> dict:
        return {
            "id": self.device_model_id,
            "name": self.name,
        }

    @staticmethod
    def delete_object(obj: "DeviceModel") -> None:
        db.session.delete(obj)
        db.session.commit()

    @classmethod
    def get_all_models(cls):
        return cls.query.all()

    @staticmethod
    def find_device_model_by_name(name: str) -> Optional["DeviceModel"]:
        return DeviceModel.filter_device_models(DeviceModel.name == name).first()

    @staticmethod
    def find_device_model_by_id(id_name: str) -> Optional["DeviceModel"]:
        return DeviceModel.filter_device_models(
            DeviceModel.device_model_id == id_name
        ).first()

    @staticmethod
    def filter_device_models(filter_: Any) -> db.Query["DeviceModel"]:
        return db.session.query(DeviceModel).filter(filter_)
