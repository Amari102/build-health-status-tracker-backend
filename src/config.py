class Config:
    DEBUG = True
    name = "your_name"
    password = "your_name"
    db_name = "build_status_db"
    SQLALCHEMY_DATABASE_URI = f'postgresql+psycopg2://{name}:{password}@localhost:5556/{db_name}'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
