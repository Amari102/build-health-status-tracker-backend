import json
from http import HTTPStatus

import flask
import flask_restful

from src.models.device_model import DeviceModel


class DeviceModelResource(flask_restful.Resource):
    def get(self, device_model_id: str):
        pass
        # Find a device model in the database. If it's found, return its data.
        # If it is not found, return 404 (double check API specs)

    def delete(self, device_model_id: int):
        device_model_to_delete = DeviceModel.find_device_model_by_id(device_model_id)

        if device_model_to_delete:
            DeviceModel.delete_object(device_model_to_delete)
            return {}, HTTPStatus.NO_CONTENT
        else:
            return {}, HTTPStatus.NOT_FOUND


class DeviceModelsListResource(flask_restful.Resource):
    def get(self):
        device_models = DeviceModel.get_all_models()
        data = [model.data() for model in device_models]

        return {"data": data}, HTTPStatus.OK

    def post(self):
        json_data = flask.request.get_json()
        # 🗒️ 9b6783 TODO: check if model is not a duplicate.
        device_model = DeviceModel(name=json_data["name"])

        device_model.save()

        return device_model.data(), HTTPStatus.CREATED


# class RecipeListResource(flask_restful.Resource):
#     def get(self):
#         data = []

#         for recipe in recipe_list:
#             if recipe.is_publish is True:
#                 data.append(recipe.data)

#         return {"data": data}, http.HTTPStatus.OK

#     def post(self):
#         data = flask.request.get_json()

#         recipe = Recipe(
#             name = data["name"],
#             description=data["description"],
#             num_of_servings = data["num_of_servings"],
#             cook_time = data["cook_time"],
#             directions = data["directions"],
#         )

#         recipe_list.append(recipe)

#         return recipe.data, http.HTTPStatus.OK


# class RecipeResource(flask_restful.Resource):
#     def get(self, recipe_id):
#         recipe = _find_recipe(recipe_id)
#         if not recipe:
#             return _recipe_not_found_error()
#         return recipe.data, http.HTTPStatus.OK

#     def put(self, recipe_id):
#         data = flask.request.get_json()

#         recipe = _find_recipe(recipe_id)
#         if not recipe:
#             return _recipe_not_found_error()

#         recipe.name = data["name"]
#         recipe.description = data["description"]
#         recipe.num_of_servings = data["num_of_servings"]
#         recipe.cook_time = data["cook_time"]
#         recipe.directions = data["directions"]

#         return recipe.data, http.HTTPStatus.OK

# class RecipePublishResource(flask_restful.Resource):
#     def put(self, recipe_id):
#         # Note: PUT method here is not necessarily used for update.
#         recipe = _find_recipe(recipe_id)
#         if not recipe:
#             return _recipe_not_found_error()

#         recipe.is_publish = True
#         return {}, http.HTTPStatus.NO_CONTENT

#     def delete(self, recipe_id):
#         # Note: DELETE method here is not necessarily used for delete.
#         recipe = _find_recipe(recipe_id)
#         if not recipe:
#             return _recipe_not_found_error()

#         recipe.is_publish = False
#         return {}, http.HTTPStatus.NO_CONTENT
#         recipe.is_publish = False
#         return {}, http.HTTPStatus.NO_CONTENT
